package com.android.cookbook;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.cookbook.model.Cake;

public class CakeDetailActivity extends AppCompatActivity {

    public static final String EXTRA_CAKES_ID = "cakesId";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cake_detail);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        int cakeId = (Integer) getIntent().getExtras().get(EXTRA_CAKES_ID);
        String cakesName = Cake.cakes[cakeId].getName();
        TextView titleTV = (TextView) findViewById(R.id.cake_text);
        titleTV.setText(cakesName);
        int cakeImage = Cake.cakes[cakeId].getImageResourceId();
        ImageView imageView = (ImageView) findViewById(R.id.cake_image);
        imageView.setImageDrawable(ContextCompat.getDrawable(this, cakeImage));
        imageView.setContentDescription(cakesName);
        int cakeRecipe = Cake.cakes[cakeId].getRecipe();
        TextView recipeTV = (TextView) findViewById(R.id.cake_recipe_text);
        recipeTV.setText(cakeRecipe);
    }
}
