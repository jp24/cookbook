package com.android.cookbook;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.cookbook.model.Soup;

public class SoupDetailActivity extends AppCompatActivity {

    public static final String EXTRA_SOUP_ID = "soupId";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_soup_detail);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        int soupId = (Integer)getIntent().getExtras().get(EXTRA_SOUP_ID);
        String soupName = Soup.soups[soupId].getName();
        TextView titleTV = (TextView) findViewById(R.id.soup_text);
        titleTV.setText(soupName);
        int soupImage = Soup.soups[soupId].getImageResourceId();
        ImageView imageView = (ImageView) findViewById(R.id.soup_image);
        imageView.setImageDrawable(ContextCompat.getDrawable(this, soupImage));
        imageView.setContentDescription(soupName);
        int soupRecipe = Soup.soups[soupId].getRecipe();
        TextView recipeTV =(TextView) findViewById(R.id.soup_recipe_text);
        recipeTV.setText(soupRecipe);
    }
}
