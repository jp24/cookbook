package com.android.cookbook;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.cookbook.model.MeatDish;

import org.w3c.dom.Text;

public class MeatDishDetailActivity extends AppCompatActivity {

    public static final String EXTRA_MEAT_DISHES_ID = "meatDishesId";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_meat_dishes_detail);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        int meatDishId = (Integer)getIntent().getExtras().get(EXTRA_MEAT_DISHES_ID);
        String meatDishName = MeatDish.meatDishes[meatDishId].getName();
        TextView titleTV = (TextView)findViewById(R.id.meat_dish_text);
        titleTV.setText(meatDishName);
        int meatDishImage = MeatDish.meatDishes[meatDishId].getImageResourceId();
        ImageView imageView = (ImageView)findViewById(R.id.meat_dish_image);
        imageView.setImageDrawable(ContextCompat.getDrawable(this, meatDishImage));
        imageView.setContentDescription(meatDishName);
        int meatDishRecipe = MeatDish.meatDishes[meatDishId].getRecipe();
        TextView recipeTV = (TextView)findViewById(R.id.meat_dish_recipe_text);
        recipeTV.setText(meatDishRecipe);
    }
}
