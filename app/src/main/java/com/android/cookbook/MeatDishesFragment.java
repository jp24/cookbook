package com.android.cookbook;

import android.content.Intent;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.cookbook.adapters.CaptionedImagesAdapter;
import com.android.cookbook.model.MeatDish;


public class MeatDishesFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        RecyclerView meatDishesRecycler = (RecyclerView)inflater.inflate(R.layout.fragment_meat_dishes,
                container, false);

        String[] meatDishesNames = new String[MeatDish.meatDishes.length];
        for(int i = 0; i < meatDishesNames.length; ++i)
            meatDishesNames[i] = MeatDish.meatDishes[i].getName();


        int[] meatDishesImages = new int[MeatDish.meatDishes.length];
        for(int i = 0; i < meatDishesImages.length; ++i)
            meatDishesImages[i] = MeatDish.meatDishes[i].getImageResourceId();

        CaptionedImagesAdapter adapter = new CaptionedImagesAdapter(meatDishesNames, meatDishesImages);
        meatDishesRecycler.setAdapter(adapter);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 2);
        meatDishesRecycler.setLayoutManager(gridLayoutManager);

        adapter.setListener(new CaptionedImagesAdapter.Listener() {
            public void onClick(int position) {
                Intent intent = new Intent(getActivity(), MeatDishDetailActivity.class);
                intent.putExtra(MeatDishDetailActivity.EXTRA_MEAT_DISHES_ID, position);
                getActivity().startActivity(intent);
            }
        });

        return meatDishesRecycler;
    }
}
