package com.android.cookbook.model;

import com.android.cookbook.R;

public class Cake extends Dish {
    public static final Dish[] cakes = {new Dish("Jabłecznik", R.drawable.jablecznik,
            R.string.recipe_jablecznik_text), new Dish("Sernik", R.drawable.sernik, R.string.recipe_sernik_text)};
}
