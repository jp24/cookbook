package com.android.cookbook.model;

import com.android.cookbook.R;

public class Soup extends Dish {
    public static final Dish[] soups = {new Dish("Rosół", R.drawable.rosol,
            R.string.recipe_rosol_text), new Dish("Pomidorowa", R.drawable.pomidorowa, R.string.recipe_pomidorowa_text)};
}
