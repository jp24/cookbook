package com.android.cookbook.model;

public class Dish {
    private String name;
    private int imageResourceId;
    private int recipe;

    public Dish() {
    }

    public Dish(String name, int imageResourceId, int recipe) {
        this.name = name;
        this.imageResourceId = imageResourceId;
        this.recipe = recipe;
    }

    public int getRecipe() {
        return recipe;
    }

    public String getName() {
        return name;
    }

    public int getImageResourceId() {
        return imageResourceId;
    }

    public void setRecipe(int recipe) {
        this.recipe = recipe;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setImageResourceId(int imageResourceId) {
        this.imageResourceId = imageResourceId;
    }
}
