package com.android.cookbook.model;

import com.android.cookbook.R;

public class MeatDish extends Dish{
    public static final Dish[] meatDishes = {new Dish("Kotlet schabowy", R.drawable.schabowy, R.string.recipe_schabowy_text),
            new Dish("Filet drobiowy", R.drawable.filet_drobiowy, R.string.recipe_filet_text)};
}
