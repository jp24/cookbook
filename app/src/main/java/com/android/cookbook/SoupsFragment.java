package com.android.cookbook;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.cookbook.adapters.CaptionedImagesAdapter;
import com.android.cookbook.model.Soup;


public class SoupsFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        RecyclerView soupRecycler = (RecyclerView) inflater.inflate(R.layout.fragment_soups,
                container, false);

        String[] soupsNames = new String[Soup.soups.length];
        for(int i = 0; i < soupsNames.length; ++i)
            soupsNames[i] = Soup.soups[i].getName();


        int[] soupsImages = new int[Soup.soups.length];
        for(int i = 0; i < soupsImages.length; ++i)
            soupsImages[i] = Soup.soups[i].getImageResourceId();

        CaptionedImagesAdapter adapter = new CaptionedImagesAdapter(soupsNames, soupsImages);
        soupRecycler.setAdapter(adapter);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 2);
        soupRecycler.setLayoutManager(gridLayoutManager);

        adapter.setListener(new CaptionedImagesAdapter.Listener() {
            @Override
            public void onClick(int position) {
                Intent intent = new Intent(getActivity(), SoupDetailActivity.class);
                intent.putExtra(SoupDetailActivity.EXTRA_SOUP_ID, position);
                getActivity().startActivity(intent);
            }
        });

        return soupRecycler;
    }
}
