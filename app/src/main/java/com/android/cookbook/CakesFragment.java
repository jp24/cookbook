package com.android.cookbook;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.cookbook.adapters.CaptionedImagesAdapter;
import com.android.cookbook.model.Cake;

public class CakesFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        RecyclerView cakesRecycler = (RecyclerView)inflater.inflate(R.layout.fragment_cakes,
                container, false);

        String[] cakesNames = new String[Cake.cakes.length];
        for(int i = 0; i < cakesNames.length; ++i)
            cakesNames[i] = Cake.cakes[i].getName();


        int[] cakesImages = new int[Cake.cakes.length];
        for(int i = 0; i < cakesImages.length; ++i)
            cakesImages[i] = Cake.cakes[i].getImageResourceId();

        CaptionedImagesAdapter adapter = new CaptionedImagesAdapter(cakesNames, cakesImages);
        cakesRecycler.setAdapter(adapter);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 2);
        cakesRecycler.setLayoutManager(gridLayoutManager);

        adapter.setListener(new CaptionedImagesAdapter.Listener() {
            public void onClick(int position) {
                Intent intent = new Intent(getActivity(), CakeDetailActivity.class);
                intent.putExtra(CakeDetailActivity.EXTRA_CAKES_ID, position);
                getActivity().startActivity(intent);
            }
        });

        return cakesRecycler;
    }
}
